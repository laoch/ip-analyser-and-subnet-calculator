# IP Analyser and Subnet calculator

**subnetcalc.py** is a handy IP Analyser and Subnet Calculator tool for IPv4 and IPv6. It is written in the Python3 programming language and requires python 3.6 or higher.

On GNU/Linux simply copy the **subnetcalc.py** file to the user ~/bin/ directory and execute as shown. On Windows I guess you have to prepend each command with **python**. 

It can analyse and IP address or subnet and return data on it, additionally subnets from each network can be generated to the depth required. The embedded *help* function lists the options available.

```
~ $ subnetcalc.py --help

  [ Help ]
  --------

  subnetcalc.py [OPTIONS]

    OPTIONS:
      IP Address or Network   : --ipaddr,-i <IP address>
      Subnet mask (IPv4 only) : --mask,-m <mask>
      Subnets prefix          : --subnet, -s <Subnet prefix>
      Include reverse pointer : --reverse-pointer, -r
      Help                    : --help, -h
```

Consider the output for an IP address with a subnet mask. The IP address is broken down into its parts and status information on the particular address is given.

```
~ $ subnetcalc.py  -i 196.203.4.67 -m 255.255.255.0

Summary for 196.203.4.67/255.255.255.0
--------------------------------------
IP Version  : 4
IP Prefix   : 24
IP Address  : 196.203.4.67/24
IP Network  : 196.203.4.0/24
Address Status: Global
```

It is also possible to add the subnetmask using slash notation.

```
~ $ subnetcalc.py  -i 196.203.4.67/255.255.255.0

Summary for 196.203.4.67/255.255.255.0
--------------------------------------
IP Version  : 4
IP Prefix   : 24
IP Address  : 196.203.4.67/24
IP Network  : 196.203.4.0/24
Address Status: Global
```

Alternatively Classless Inter Domain Routing (CIDR) format can be applied.

```
~ $ subnetcalc.py  -i 196.203.4.67/24

Summary for 196.203.4.67/24
---------------------------
IP Version  : 4
IP Prefix   : 24
IP Address  : 196.203.4.67/24
IP Network  : 196.203.4.0/24
Address Status: Global
```

The Reverse Pointer address can be requested of the IP is an address and not a network.

```
~ $ subnetcalc.py  -i 196.203.4.67/24 -r

Summary for 196.203.4.67/24
---------------------------
IP Version      : 4
IP Prefix       : 24
IP Address      : 196.203.4.67/24
Reverse pointer : 67.4.203.196.in-addr.arpa
IP Network      : 196.203.4.0/24
Address Status  : Global

~ $ subnetcalc.py  -i 2002:5:192::/60 -r

Summary for 2002:5:192::/60
---------------------------
IP Version      : 6
IP Prefix       : 60
IP Network      : 2002:5:192::/60
Reverse pointer : 0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.2.9.1.0.5.0.0.0.2.0.0.2.ip6.arpa
IP Network      : 2002:5:192::/60
Address Status  : Global
```

It works equally well with IPv6 addresses and will automatically compress IPv6 addresses in the output.

```
~ $ subnetcalc.py  -i 2001:db8::/64

Summary for 2001:db8::/64
-------------------------
IP Version  : 6
IP Prefix   : 64
IP Network  : 2001:db8::/64
IP Network  : 2001:db8::/64
Address Status: Documentation, Private

dobriain@riomhaire-E550 ~ $ subnetcalc.py  -i ff02:2::/64

Summary for ff02:2::/64
-----------------------
IP Version  : 6
IP Prefix   : 64
IP Network  : ff02:2::/64
IP Network  : ff02:2::/64
Address Status: Multicast, Global

~ $ subnetcalc.py  -i 2002:0005:0192:0000::/60 

Summary for 2002:0005:0192:0000::/60
------------------------------------
IP Version  : 6
IP Prefix   : 60
IP Network  : 2002:5:192::/60
IP Network  : 2002:5:192::/60
Address Status: Global
```

Calculating subnets for a prefix that is longer than the prefix given with the IP address can be calculated too. 


```
~ $ subnetcalc.py  -i 196.203.4.0/24 -s 26

Summary for 196.203.4.0/24
--------------------------
IP Version  : 4
IP Prefix   : 24
IP Network  : 196.203.4.0/24
IP Network  : 196.203.4.0/24
Address Status: Global

Are you ready for the subnets? 
Subnets for 196.203.4.0/24
--------------------------
196.203.4.0/26
196.203.4.64/26
196.203.4.128/26
196.203.4.192/26

```
and here is an example for IPv6.

```
~ $ subnetcalc.py  -i 2002:5:192::/60 -s 64

Summary for 2002:5:192::/60
---------------------------
IP Version  : 6
IP Prefix   : 60
IP Network  : 2002:5:192::/60
IP Network  : 2002:5:192::/60
Address Status: Global

Are you ready for the subnets? 
Subnets for 2002:5:192::/60
---------------------------
2002:5:192::/64
2002:5:192:1::/64
2002:5:192:2::/64
2002:5:192:3::/64
2002:5:192:4::/64
2002:5:192:5::/64
2002:5:192:6::/64
2002:5:192:7::/64
2002:5:192:8::/64
2002:5:192:9::/64
2002:5:192:a::/64
2002:5:192:b::/64
2002:5:192:c::/64
2002:5:192:d::/64
2002:5:192:e::/64
2002:5:192:f::/64
```


