#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" IP analyser and Subnet Calculator """

__author__      = "Diarmuid O'Briain"
__copyright__   = "Copyright 2019, C²S Consulting"
__license__     = "European Union Public Licence v1.2"
__version__     = "Version 1.0"

import sys
import getopt
import ipaddress as ipa

pl = {'4': 32 + 1, '6': 128 + 1}

def error(err_name='[ Help ]\n  --------'):
    ''' Function to handle errors '''

    name = sys.argv[0].split('/')[-1]
    print(f"\n  {err_name}\n")
    print(f'  {name} [OPTIONS]\n'
          f'\n    OPTIONS:\n'
          f'      IP Address or Network   : --ipaddr,-i <IP address>\n'
          f'      Subnet mask (IPv4 only) : --mask,-m <mask>\n'
          f'      Subnets prefix          : --subnet, -s <Subnet prefix>\n'
          f'      Include reverse pointer : --reverse-pointer, -r\n'
          f'      Help                    : --help, -h\n')
    sys.exit(1)

    # End of error() function

def validate_ip_net(args):
    ''' Function to validate IP Networks and IP addresses '''

    l_ = list()
    d_ = dict()
    s_ = str()
    f_ = dict()

    # Split on '/' and test for subnet or prefix
    l_.extend(args.split('/')) if ('/' in args) else l_.append(args)
    if (len(l_) < 2):
        error("ERROR: No subnetmask or prefix given") 

    # If subnet mask convert to CIDR format
    if ('.' in l_[1]):
        try:
            d_['prefix'] = ipa.IPv4Network(f'0.0.0.0/{l_[1]}').prefixlen
        except:
            error(f"ERROR: '{l_[1]}' is not a valid subnetmask")
    elif (l_[1].isdigit()):
        d_['prefix'] = int(l_[1])

    else:
        error(f"ERROR: '{l_[1]}' is not a valid subnetmask or prefix")  

    # Get IP address version
    try:
        d_['ver'] = ipa.ip_address(l_[0]).version
    except:
        error(f"ERROR: {l_[0]} does not appear to be an IPv4 or IPv6 address.")

    # Test prefix length is valid
    for x in [4,6]:
        if (d_['ver'] == x and d_['prefix'] not in range(pl[f'{x}'])):
            error(f"ERROR: {d_['prefix']} out of range for IPv{x}")
     
    # Assign IP to dict
    d_['ip'] = ipa.ip_interface(f"{l_[0]}/{d_['prefix']}")
    d_['net'] = d_['ip'].network

    # Get address status
    if ('2001:db8' in str(d_['ip'])) or ('2001:DB8' in str(d_['ip'])):
        f_['Documentation'] = True
    else:
        f_['Documentation'] = False
    f_['Multicast'] = d_['ip'].is_multicast
    f_['Global'] = d_['ip'].is_global
    f_['Unspecified'] = d_['ip'].is_unspecified
    f_['Reserved'] = d_['ip'].is_reserved
    f_['Loopback'] = d_['ip'].is_loopback
    f_['Link_local'] = d_['ip'].is_link_local
    f_['Private'] = d_['ip'].is_private
    d_['flags'] = f_

    # Get reverse pointer
    s_,_ = str(d_['ip']).split('/')
    d_['rpa'] = ipa.ip_address(s_).reverse_pointer

    return(d_)

    # End of validate_ip_net() function

def subnet_gen(*args):
    ''' Generate subnets for given IP network '''

    for x in ipa.ip_network(args[0]).subnets(new_prefix=args[1]):
        yield(x)

    # End of subnet_gen() generator

def main():
    ''' Main function '''

    list_ = list()
    list2_ = list()
    dict_ = dict()
    dict2_ = dict()    
    reverse_pointer = False
    subnet_bits = int()
    argv = sys.argv[1:]

    try:
        opts_ = ["help", "reverse-pointer", "ipaddr=", "mask=","subnet="]
        opts, args = getopt.getopt(argv,"hri:m:s:",opts_)
    except getopt.GetoptError:
        error("ERROR: Incorrect switches used.")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            error()
            sys.exit(0)
        elif opt in ("-r", "--reverse-pointer"):
            reverse_pointer = True
        elif opt in ("-i", "--ipaddr"):
            dict_['ipaddr'] = arg
        elif opt in ("-m", "--mask"):
            dict_['mask'] = arg
        elif opt in ("-s", "--subnet"):
            dict_['subnet'] = arg
        else:
            assert False, "ERROR: Not sure how we arrived here"

    # Test for prefix and mask
    if ('ipaddr' in dict_.keys()) and ('mask' in dict_.keys()):
        if ('/' in dict_['ipaddr']):
            error("ERROR: No '--mask' required if prefix given.")
            sys.exit(1)
        else:
            ip_ = f"{dict_['ipaddr']}/{dict_['mask']}"
    elif('ipaddr' in dict_.keys()):
        ip_ = f"{dict_['ipaddr']}"
    else:
        error("ERROR: No IP address given")

    # Test IP address
    dict2_ = validate_ip_net(ip_)

    # Test the subnet if given
    if ('subnet' in dict_.keys()):
        if not (int(dict_['subnet']) in range(dict2_['prefix'], pl[f"{int(dict2_['ver'])}"])):
            error(f"ERROR: Subnet '{dict_['subnet']}' out of scope "
                  f"between prefix '{int(dict2_['prefix'])}' "
                  f"and '{pl[str(dict2_['ver'])] - 1}'.")

    # Print IP details 
    if (reverse_pointer == True):
        pad = 16
    else:
        pad = 12
    summary = f"Summary for {ip_}"
    print(f"\n{summary}\n{'-' * len(summary)}")
    print(f"{'IP Version':<{pad}}: {dict2_['ver']}\n"
          f"{'IP Prefix':<{pad}}: {dict2_['prefix']}")

    if (str(dict2_['ip']) == str(dict2_['net'])):
        print(f"{'IP Network':<{pad}}: {dict2_['net']}")
    else:
        print(f"{'IP Address':<{pad}}: {dict2_['ip']}")

    if(reverse_pointer):
        print(f"{'Reverse pointer':<{pad}}: {dict2_['rpa']}")

    print(f"{'IP Network':<{pad}}: {dict2_['net']}")
     
    # Print flags with a status of True
    for k,v in dict2_['flags'].items():
        if (v == True):
            list2_.append(k)

    print(f"{'Address Status':<{pad}}: {', '.join(list2_)}\n")

    # Test if subnets are required
    if not ('subnet' in dict_):
        sys.exit(0)
    else:
        input("Are you ready for the subnets?")

    # Print IP subnet details  
    subnet_title = f"Subnets for {dict2_['net']}"
    print(f"{subnet_title}\n{'-' * len(subnet_title)}")

    ip_subnets = subnet_gen(dict2_['net'], int(f"{dict_['subnet']}"))

    while(ip_subnets):
        try: 
            print(next(ip_subnets))
        except StopIteration:
            break

    # End of main() function

if __name__ == '__main__':
    if (sys.version_info.major == 3 and sys.version_info.minor >= 6):
        main()
    else:
        print('A python version greater that 3.5 is required.')
else:
    exit(1)